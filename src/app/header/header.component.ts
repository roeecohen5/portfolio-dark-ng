import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @ViewChild('click_here_btn')
  private clickHereBtn: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

  toExp(): void
  {
    this.clickHereBtn.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }


}
