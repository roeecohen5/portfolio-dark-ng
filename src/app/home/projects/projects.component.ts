import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  @ViewChild('lem_video') lem: ElementRef;
  @ViewChild('cook_video') cook: ElementRef;

  constructor() {
  }

  ngOnInit(): void {
    $('#videoLemModal').on('hidden.bs.modal', () => {
      this.lem.nativeElement.innerHTML = `<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/cJwQ8QGgGxE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>></iframe>`;

    });

    $('#videoCookModal').on('hidden.bs.modal', () => {
      this.cook.nativeElement.innerHTML = `<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/I2rxy0oFdBQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>></iframe>`;
    });
  }

  openModal(): void {
    $('#exampleModalLong').modal('show');
  }

}
