import {Component, OnInit, ViewEncapsulation} from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-contact-stickey',
  templateUrl: './contact-stickey.component.html',
  styleUrls: ['./contact-stickey.component.css'],
  encapsulation: ViewEncapsulation.None,

})
export class ContactStickeyComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
    $('#contact_info').hide();
    $('lottie-player').on('click', () => {
      $('#contact_info').fadeToggle(500);
    });
  }

}
