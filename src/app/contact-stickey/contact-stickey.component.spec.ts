import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactStickeyComponent } from './contact-stickey.component';

describe('ContactStickeyComponent', () => {
  let component: ContactStickeyComponent;
  let fixture: ComponentFixture<ContactStickeyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactStickeyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactStickeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
