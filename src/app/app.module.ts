import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { Routes, RouterModule } from '@angular/router';
import { EducationComponent } from './home/education/education.component';
import { ProjectsComponent } from './home/projects/projects.component';
import { ExperienceComponent } from './home/experience/experience.component';
import { SkillsComponent } from './home/skills/skills.component';
import { MilitaryComponent } from './home/military/military.component';
import { ContactStickeyComponent } from './contact-stickey/contact-stickey.component';
import { FooterComponent } from './footer/footer.component';
import {NgsRevealModule} from 'ngx-scrollreveal';

const routes: Routes = [
  { path: 'first-component', component: AppComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    EducationComponent,
    ProjectsComponent,
    ExperienceComponent,
    SkillsComponent,
    MilitaryComponent,
    ContactStickeyComponent,
    FooterComponent
  ],
  imports: [BrowserModule, RouterModule.forRoot(routes), NgsRevealModule],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]

})
export class AppModule { }
